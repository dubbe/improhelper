// app.js 
var express =   require('express');
var app     =   express();
var port    =   process.env.PORT || 8080;

 
var port = process.env.PORT || 8080;

app.get('/sample', function(req, res) {
    res.send('this is a sample!');  
}); 
 
//you won't need 'connect-livereload' if you have livereload plugin for your browser 
app.use(require('connect-livereload')());

// we'll create our routes here

var admin = require('./routes/admin')(express);

app.use('/admin/', admin);

app.listen(port, function() {
    console.log("Express server listening on port %d in %s mode", port, process.env.IP); 
})