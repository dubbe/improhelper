'use strict';

var gulp    = require( 'gulp' ),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    async = require('async'),
    uglify = require('gulp-uglify'),
    react = require('gulp-react'),
    htmlreplace = require('gulp-html-replace'),
    tinyLr = require('tiny-lr'),
    fork = require('child_process').fork,
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    reactify = require('reactify'),
    streamify = require('gulp-streamify');

var path = {
    HTML: 'app/src/*.html',
    //ALL: ['app/src/js/*.js', 'app/src/js/**/*.js', 'app/src/*.html'],
    //JS: ['app/src/js/*.js', 'app/src/js/**/*.js'],
    MINIFIED_OUT: 'improhelper.min.js',
    OUT: 'improhelper.js',
    DEST_SRC: 'app/src',
    DEST_BUILD: 'app/build',
    DEST: 'app/build',
    ENTRY_POINT: './app/src/js/src/app.js'
};

var dirs = {
    app: [
        'views/{,*/}*.jade',
        'routes/{,*/}*.js',
        'models/{,*/}*.js',
        'libs/{,*/}*.js',
        'server.js',
    ],
    public: [
        'public/scripts/{,*/}*.js',
        'public/styles/{,*/}*.css',
        'public/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
    ]
};

var livereload = {
    instance: null,

    port: 35729,

    start: function( callback ) {
        livereload.instance = tinyLr();

        livereload.instance.listen( livereload.port, callback );
    },

    changed: function( event, callback ) {
        var filepath = event.path;

        livereload.instance.changed({
            body: {
                files: [ filepath ]
            }
        });
        if( callback ) callback();
    }
};

var app = {
    instance: {},

    path: 'server.js',

    env: { NODE_ENV: 'development', port: 3000 },

    start: function( callback ) {
        process.execArgv.push( '--harmony' );

        app.instance = fork( app.path, { silent: true, env: app.env } );
        app.instance.stdout.pipe( process.stdout );
        app.instance.stderr.pipe( process.stderr );

        gutil.log( gutil.colors.cyan( 'Starting' ), 'express server ( PID:', app.instance.pid, ')' );

        if( callback ) callback();
    },

    stop: function( callback ) {
        if( app.instance.connected ) {
            app.instance.on( 'exit', function() {
                gutil.log( gutil.colors.red( 'Stopping' ), 'express server ( PID:', app.instance.pid, ')' );
                if( callback ) callback();
            });
            return app.instance.kill( 'SIGINT' );
        }
        if( callback ) callback();
    },

    restart: function( event ) {
        async.series([
            app.stop,
            app.start,
            function( callback ) {
                livereload.changed( event, callback );
            }
        ]);
    }
};


gulp.task( 'server', function( callback ) {
    async.series([
        app.start,
        livereload.start
    ], callback );
});


/*gulp.task('copy', function(){
  gulp.src(path.HTML)
    .pipe(gulp.dest(path.DEST_SRC));
});*/

gulp.task( 'watch', function() {
    gulp.watch(dirs.app, app.restart);
    gulp.watch(dirs.public, livereload.changed);
   // gulp.watch(path.HTML, ['copy']);

    var watcher  = watchify(browserify({
        entries: [path.ENTRY_POINT],
        transform: [reactify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    }));

    return watcher.on('update', function () {
        watcher.bundle()
            .pipe(source(path.OUT))
            .pipe(gulp.dest(path.DEST_SRC + "/js"))
            console.log('Updated');
    })
    .bundle()
    .pipe(source(path.OUT))
    .pipe(gulp.dest(path.DEST_SRC));
});

gulp.task('build', function(){
  browserify({
    entries: [path.ENTRY_POINT],
    transform: [reactify]
  })
    .bundle()
    .pipe(source(path.MINIFIED_OUT))
    //.pipe(streamify(uglify(path.MINIFIED_OUT)))
    .pipe(gulp.dest(path.DEST_BUILD + '/js/'));
});

gulp.task('replaceHTML', function(){
  gulp.src(path.HTML)
    .pipe(htmlreplace({
      'js': 'js/' + path.MINIFIED_OUT
    }))
    .pipe(gulp.dest(path.DEST));
});

gulp.task( 'default', [ 'server', 'watch' ] );

gulp.task('production', ['replaceHTML', 'build']);