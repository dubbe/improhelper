

module.exports  = function (express) {
    // get an instance of router
    var router = express.Router();
    var path = require('path');

    // route middleware that will happen on every request
    router.use(function(req, res, next) {

        // log each request to the console
        console.log(req.method, req.url);

        // continue doing what we were doing and go to the route
        next();
    });

    // home page route (http://localhost:8080)
    router.get('/', function(req, res) {
        res.sendFile(path.join(__dirname +'/../app/src/admin.html'));
    });


    return router;
};
